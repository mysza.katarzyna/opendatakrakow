import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';

import { ThemeProvider } from 'styled-components';

import theme from './styles/mainTheme.css';
import GlobalStyle from './styles/GlobalStyle.css';
import { Wrapper } from './styles/index.css';

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import MapComponent from './components/MapComponent/MapComponent.js';
import About from './components/About/About';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Router>
        <Header />
        <Wrapper>
          <Switch>
            <Route exact path='/' component={MapComponent} />
            <Route exact path='/about' component={About} />
            <Route render={() => <Redirect to='/' />} />
          </Switch>
        </Wrapper>
        <Footer />
      </Router>
    </ThemeProvider>
  );
}

export default App;
