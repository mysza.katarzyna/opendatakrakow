import React from 'react';
import * as S from './About.css';
import ReactIcon from '../../assets/react.png';
import StyledIcon from '../../assets/styledcomp2.png';
import LeafletIcon from '../../assets/leaflet.png';
import OsmIcon from '../../assets/osm2.png';
import JsIcon from '../../assets/js.png';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  variableWidth: true,
  autoplay: true,
  speed: 3000,
  autoplaySpeed: 3000,
  cssEase: 'linear',
};

const About = () => {
  return (
    <S.Wrapper>
      <S.Title>About the project:</S.Title>
      <S.Description>
        The goal of the project was visualisation of Open Data from the city of
        Krakow. To achieve that, I decided to create a React application with
        Leaflet library, that allowed me to display JSON and GeoJSON data on the
        map(provided by openstreetmap), using custom markers and pop ups. I also
        used styled-components for smooth and easy styling.
      </S.Description>
      <S.UsedTechnologies>
        <Slider {...settings}>
          <S.LogoContainer>
            <img src={ReactIcon} alt='react logo' />
          </S.LogoContainer>
          <S.LogoContainer>
            <img src={JsIcon} alt='javascript logo' />
          </S.LogoContainer>
          <S.LogoContainer>
            <img src={StyledIcon} alt='styled components logo' />
          </S.LogoContainer>
          <S.LogoContainer>
            <img src={LeafletIcon} alt='leaflet logo' />
          </S.LogoContainer>
          <S.LogoContainer>
            <img src={OsmIcon} alt='openstreetmap logo' />
          </S.LogoContainer>
        </Slider>
      </S.UsedTechnologies>
    </S.Wrapper>
  );
};

export default About;
