import styled from 'styled-components';

export const Wrapper = styled.div`
  min-height: 40vh;
  width: 60%;

  .slick-track {
    margin: auto;
  }
`;

export const Title = styled.h2`
  color: ${({ theme }) => theme.text};
`;

export const Description = styled.div``;

export const UsedTechnologies = styled.div`
  margin: 50px 0;
`;

export const LogoContainer = styled.div`
  margin: 0 30px;
`;

export const Summary = styled.div``;
