import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Wrapper = styled.header`
  position: sticky;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  min-height: 35vh;
  background: ${({ theme }) => theme.bgLight};
`;

export const Title = styled.h1`
  color: #ffe66d;
  text-align: center;
  font-family: 'Comfortaa';
  font-size: 82px;
  font-weight: normal;
  margin: 1vh;
`;

export const Description = styled.p`
  color: #f7fff7;
  text-align: center;
  font-family: 'Open Sans';
  font-size: 22px;
  width: 70%;
`;

export const Menu = styled.menu`
  display: flex;
  width: 100%;
  justify-content: center;
  height: 40px;
  padding: 0;
`;

export const MenuItem = styled(Link)`
  position: relative;
  width: 100px;
  text-align: center;
  border: none;
  background: none;
  cursor: pointer;
  color: ${({ theme }) => theme.links};
  text-decoration: none;
  padding: 10px 20px;

  :after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
    width: 20%;
    border-bottom: 1px transparent solid;
    transition: 1s;
  }
  :not(:last-child) {
    border-right: 1px solid ${({ theme }) => theme.borderLight};
  }

  :hover {
    :after {
      border-bottom: 1px solid ${({ theme }) => theme.links};
      width: 70%;
    }
  }
`;
