import React from 'react';
import { Wrapper, Title, Description, Menu, MenuItem } from './Header.css';

const Header = () => {
  return (
    <Wrapper>
      <Title>Open Maps</Title>
      <Description>
        Selection of the open data from the city of Krakow
      </Description>
      <Menu>
        <MenuItem to='/'>Maps</MenuItem>
        <MenuItem to='/about'>About</MenuItem>
      </Menu>
    </Wrapper>
  );
};

export default Header;
