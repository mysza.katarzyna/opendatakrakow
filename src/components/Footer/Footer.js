import React from 'react';
import {
  Wrapper,
  Section,
  Copyright,
  Logo,
  ImgLink,
  ExtLink,
  SectionTitle,
} from './Footer.css.js';
import UpLogo from '../../assets/uplogo.png';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

const Footer = () => {
  return (
    <Wrapper>
      <Section>
        <SectionTitle>External links:</SectionTitle>
        <ExtLink
          href='https://msip.krakow.pl/'
          target='_blank'
          rel='noopener noreferrer'
        >
          MSIP Krakow
          <OpenInNewIcon />
        </ExtLink>
        <ExtLink
          href='https://opendatahandbook.org/guide/en/what-is-open-data/'
          target='_blank'
          rel='noopener noreferrer'
        >
          Open Data Handbook <OpenInNewIcon />
        </ExtLink>
      </Section>
      <Section>
        <Copyright>Katarzyna Mysza © Open Data Krakow</Copyright>
      </Section>
      <Section>
        <ImgLink
          href='https://up.krakow.pl'
          target='_blank'
          rel='noopener norefferer'
        >
          <Logo src={UpLogo} alt='logo UP' />
        </ImgLink>
      </Section>
    </Wrapper>
  );
};

export default Footer;
