import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  height: 25vh;
  background: ${({ theme }) => theme.bgLight};
  display: flex;

  @media (max-width: 768px) {
    height: auto;
    min-height: 200px;
    flex-wrap: wrap;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;

export const Section = styled.div`
  padding: 20px;
  height: 100%;
  width: 33%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: ${({ theme }) => theme.primary};

  @media (max-width: 768px) {
    width: 100%;
  }
`;

export const Copyright = styled.span`
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme.primary};
`;

export const ImgLink = styled.a`
  height: 90%;
`;

export const Logo = styled.img`
  height: 100%;
  max-height: 150px;
`;

export const ExtLink = styled.a`
  position: relative;
  text-align: center;
  border: none;
  background: none;
  cursor: pointer;
  color: ${({ theme }) => theme.links};
  text-decoration: none;
  padding: 5px 0;
  display: flex;
  align-items: center;

  :after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
    width: 20%;
    border-bottom: 1px transparent solid;
    transition: 1s;
  }

  :hover {
    :after {
      border-bottom: 1px solid ${({ theme }) => theme.links};
      width: 100%;
    }
  }

  svg {
    margin-left: 5px;
    height: 16px;
  }
`;

export const SectionTitle = styled.p`
  font-weight: 600;
`;
