import React, { useState, useEffect } from 'react';
import { MapContainer, TileLayer, Marker, Tooltip } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import {
  Wrapper,
  Filters,
  Details,
  DetailsHeading,
  DetailsParagraph,
  Desc,
  SelectWrap,
} from './MapComponent.css';
import Select from 'react-select';

import { mapPoints } from './MapMarkers';
import { options, groupedOptions } from './options';
import { ReactComponent as InfoIcon } from '../../assets/info-circle.svg';

const MapComponent = () => {
  const [selectedValue, setSelectedValue] = useState();
  const [userCoords, setUserCoords] = useState([0, 0]);
  const handleChange = e => {
    setSelectedValue(e.value);
  };

  // get user location
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(position => {
      setUserCoords([position.coords.latitude, position.coords.longitude]);
    });
  }, []);

  const groupStyles = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    fontSize: 10,
  };
  const groupBadgeStyles = {
    backgroundColor: '#EBECF0',
    borderRadius: '2em',
    color: '#172B4D',
    display: 'inline-block',
    fontSize: 12,
    fontWeight: 'normal',
    lineHeight: '1',
    minWidth: 1,
    padding: '0.16666666666667em 0.5em',
    textAlign: 'center',
  };

  const formatGroupLabel = data => (
    <div style={groupStyles}>
      <span>{data.label}</span>
      <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
  );

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? '#000' : '#fff',
      background: state.isSelected ? '#84DCCF' : '#343434',
      fontWeight: state.isSelected ? '600' : '400',
      '&:hover': {
        background: '#84DCCF',
        color: '#000',
        transition: '0.1s',
      },
      padding: 10,
    }),

    control: (provided, state) => ({
      ...provided,
      background: 'none',
      borderRadius: '5px',
      color: '#F7FFF7',
      borderColor: state.isFocused && '#84DCCF',
      boxShadow: state.isFocused && 'none',
      '&:hover': {
        borderColor: state.isFocused && '#84DCCF',
        boxShadow: state.isFocused && 'none',
      },
    }),

    menu: provided => ({
      ...provided,
      marginTop: '2px',
      width: 300,
      borderRadius: '0px',
      color: '#F7FFF7',
      background: '#343434',
      zIndex: '9999',
    }),

    singleValue: provided => ({
      ...provided,
      color: '#F7FFF7',
    }),
  };

  const markersByCategory = options.map((el, i) => {
    return selectedValue === options[i].value && mapPoints[i].category;
  });

  const descriptionByCategory = options.map((el, i) => {
    return (
      selectedValue === options[i].value && (
        <div key={i}>
          <DetailsHeading>{mapPoints[i].title}</DetailsHeading>
          <DetailsParagraph>{mapPoints[i].details}</DetailsParagraph>
        </div>
      )
    );
  });

  return (
    <Wrapper>
      <Filters>
        <SelectWrap>
          <Select
            options={groupedOptions}
            styles={customStyles}
            value={options.filter(obj => obj.value === selectedValue)}
            defaultValue={options[0]}
            onChange={handleChange}
            formatGroupLabel={formatGroupLabel}
          />
        </SelectWrap>
        <Desc>
          <InfoIcon
            style={{ height: '25px', fill: '#fff', marginRight: '10px' }}
          />
          <p style={{ margin: 0 }}>
            Select an option from the list to show the markers on the map. Click
            on a chosen marker to display a pop-up with details.
          </p>
        </Desc>
      </Filters>
      <MapContainer
        style={{ height: '60vh', width: '70vw' }}
        zoom={12}
        center={[50.0647, 19.945]}
        minZoom={11}
        maxBounds={[
          [50.2542, 19.46],
          [49.915, 20.407],
        ]}
        maxBoundsViscosity={1}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        />
        <Marker position={[userCoords[0], userCoords[1]]}>
          <Tooltip direction='bottom' offset={[-20, 30]} opacity={1}>
            You are here
          </Tooltip>
        </Marker>
        {markersByCategory}
      </MapContainer>
      <Details>
        {selectedValue === undefined ? (
          <DetailsHeading>Select an option to read more</DetailsHeading>
        ) : (
          descriptionByCategory
        )}
      </Details>
    </Wrapper>
  );
};

export default MapComponent;
