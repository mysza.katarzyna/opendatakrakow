import React from 'react';
import { GeoJSON, Marker, Popup } from 'react-leaflet';
import {
  wifiIcon,
  libraryIcon,
  busIcon,
  tramIcon,
  babyIcon,
  schoolIcon,
} from './MapIcons';
import * as geolib from 'geolib';

import OpenInNewIcon from '@material-ui/icons/OpenInNew';

const wifipointsdata = require('../../map_data/punkty_wifi.json');
const culturalparkdata = require('../../map_data/granica_parku_kulturowego.json');
const librariesdata = require('../../map_data/biblioteki.json');
const busstopsdata = require('../../map_data/przystanki_autobusowe.json');
const tramstopsdata = require('../../map_data/przystanki_tramwajowe.json');
const parkingdata = require('../../map_data/strefy_parkowania.json');
const parentandkiddata = require('../../map_data/kluby_rodzicow_z_dziecmi_do_lat_3.json');
const youthculturalcenterdata = require('../../map_data/schools/mlodziezowe_domy_kultury.json');
const musicschoolsdata = require('../../map_data/schools/szkoly_muzyczne.json');
const primaryschoolsdata = require('../../map_data/schools/szkoly_podstawowe.json');
const highschoolsdata = require('../../map_data/schools/licea_ogolnoksztalcace.json');
const vocationalschoolsdata = require('../../map_data/schools/technika.json');
const collegesdata = require('../../map_data/schools/szkoly_policealne.json');
const schoolcomplexdata = require('../../map_data/schools/zespoly_szkol_i_placowek_oswiatowych.json');
const pnurseriesdata = require('../../map_data/schools/zlobki_publiczne.json');
const npnurseriesdata = require('../../map_data/schools/zlobki_niepubliczne.json');
const kindergartensdata = require('../../map_data/schools/przedszkola_i_punkty_przedszkolne.json');
const dormitoriesdata = require('../../map_data/schools/internaty_i_bursy.json');

export const busstops = busstopsdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates].reverse()}
      icon={busIcon}
      key={el.id}
    >
      <Popup>{el.properties.stop_name}</Popup>
    </Marker>
  );
});

export const tramstops = tramstopsdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates].reverse()}
      icon={tramIcon}
      key={el.id}
    >
      <Popup>{el.properties.stop_name}</Popup>
    </Marker>
  );
});

export const wifipoints = wifipointsdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates].reverse()}
      icon={wifiIcon}
      key={el.id}
    >
      <Popup>{el.properties.lokalizacj}</Popup>
    </Marker>
  );
});

export const libraries = librariesdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={libraryIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.ULICA} {el.properties.NR_ADRES}
      </Popup>
    </Marker>
  );
});

export const parentandkid = parentandkiddata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates].reverse()}
      icon={babyIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.organizacja} <br />
        {el.properties.ulica} {el.properties.adres}
        <br />
        <a
          href={el.properties.link}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const youthculturalcenter = youthculturalcenterdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={schoolIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const musicschools = musicschoolsdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={schoolIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const primaryschools = primaryschoolsdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={schoolIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const highschools = highschoolsdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={schoolIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const vocationalschools = vocationalschoolsdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={schoolIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const pnurseries = pnurseriesdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates].reverse()}
      icon={babyIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const npnurseries = npnurseriesdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={babyIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const schoolcomplex = schoolcomplexdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={schoolIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const colleges = collegesdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={schoolIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const kindergartens = kindergartensdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={babyIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const dormitories = dormitoriesdata.features.map(el => {
  return (
    <Marker
      position={[...el.geometry.coordinates[0]].reverse()}
      icon={schoolIcon}
      key={el.id}
    >
      <Popup>
        {el.properties.NZ_PL} <br />
        {el.properties.NZ_UL} {el.properties.NR_BUD}
        <br />
        <a
          href={el.properties.www}
          target='_blank'
          rel='noreferrer'
          style={{ display: 'flex', alignItems: 'center' }}
        >
          Website <OpenInNewIcon style={{ fill: 'black', height: '1rem' }} />
        </a>
      </Popup>
    </Marker>
  );
});

export const parking = (
  <GeoJSON data={parkingdata.features} key={parkingdata.id} />
);

export const culturalpark = (
  <GeoJSON data={culturalparkdata.features} key={culturalparkdata.id} />
);

let area = geolib.getAreaOfPolygon(
  culturalparkdata?.features[0]?.geometry?.coordinates[0]
);

let a = geolib.convertArea(area, 'km2').toFixed(3);

export const mapPoints = [
  {
    category: wifipoints,
    title: 'Wifi points',
    details: `There are ${wifipointsdata?.features?.length} hotspots in Krakow.`,
  },
  {
    category: culturalpark,
    title: '"Old Town" Cultural park',
    details: `The area of the cultural park takes ${a} square kilometers. The borders run along the following streets: Straszewskiego, Podwale, Dunajewskiego, Basztowa, Westerplatte, Św. Gertrudy, Bernardyńska, a fragment of the Vistula Boulevard to Podzamcze Street connecting with Straszewskiego Street.
    `,
  },
  {
    category: libraries,
    title: 'Libraries',
    details: `There are ${librariesdata?.features?.length} libraries in Krakow.`,
  },
  {
    category: busstops,
    title: 'Bus stops',
    details: `There are ${busstopsdata?.features?.length} bus stops in Krakow.`,
  },
  {
    category: tramstops,
    title: 'Tram stops',
    details: `There are ${tramstopsdata?.features?.length} tram stops in Krakow.`,
  },
  { category: parking, title: 'Parking areas', details: 'sample text' },
  {
    category: parentandkid,
    title: 'Parents and children clubs(up to 3 years old)',
    details: `There are ${parentandkiddata?.features?.length} parents and children clubs in Krakow.`,
  },
  {
    category: youthculturalcenter,
    title: 'Youth Culture Centers',
    details: `There are ${youthculturalcenterdata?.features?.length} youth cultural centers in Krakow.`,
  },
  {
    category: musicschools,
    title: 'Music schools',
    details: `There are ${musicschoolsdata?.features?.length} music schools in Krakow.`,
  },
  {
    category: primaryschools,
    title: 'Primary schools',
    details: `There are ${primaryschoolsdata?.features?.length} primary schools in Krakow.`,
  },
  {
    category: highschools,
    title: 'High schools',
    details: `There are ${highschoolsdata?.features?.length} high schools in Krakow.`,
  },
  {
    category: vocationalschools,
    title: 'Vocational schools',
    details: `There are ${vocationalschoolsdata?.features?.length} vocational schools in Krakow.`,
  },
  {
    category: colleges,
    title: 'Colleges',
    details: `There are ${collegesdata?.features?.length} colleges in Krakow.`,
  },
  {
    category: schoolcomplex,
    title: 'School complexes',
    details: `There are ${schoolcomplexdata?.features?.length} school complex in Krakow.`,
  },
  {
    category: pnurseries,
    title: 'Public nurseries',
    details: `There are ${pnurseriesdata?.features?.length} public nurseries in Krakow.`,
  },
  {
    category: npnurseries,
    title: 'Private nurseries',
    details: `There are ${npnurseriesdata?.features?.length} private nurseries in Krakow.`,
  },
  {
    category: kindergartens,
    title: 'Kindergartens',
    details: `There are ${kindergartensdata?.features?.length} kindergartens in Krakow.`,
  },
  {
    category: dormitories,
    title: 'Dormitories',
    details: `There are ${dormitoriesdata?.features?.length} dormitories in Krakow.`,
  },
];
