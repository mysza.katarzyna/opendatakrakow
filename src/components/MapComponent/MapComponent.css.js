import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3vh;

  .leaflet-popup-content {
    color: #000;
    font-size: 0.9rem;
  }
`;

export const Filters = styled.div`
  margin: 20px 0;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  max-width: 70vw;
  @media (max-width: 768px) {
    flex-wrap: wrap;
  }
`;

export const MenuOption = styled.option`
  height: 40px;
  background: #6ca6c1;

  :hover {
    background: red;
  }
`;

export const Details = styled.div`
  width: 100%;
  max-width: 70vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 20px 0;
  padding: 10px;
  min-height: 100px;
  background: ${({ theme }) => theme.bg};
  border: 1px solid ${({ theme }) => theme.primary};
  border-radius: 10px;
`;

export const DetailsHeading = styled.h3`
  font-size: 1.2rem;
  color: ${({ theme }) => theme.secondary};
  width: 100%;
`;

export const DetailsParagraph = styled.p``;

export const Desc = styled.div`
  font-size: 14px;
  display: flex;
  align-items: center;
  border: 1px solid ${({ theme }) => theme.primary};
  border-radius: 10px;
  padding: 6px 15px;
  margin-left: 40px;

  @media (max-width: 768px) {
    margin: 20px 0;

    p {
      width: 80%;
    }
  }
`;

export const SelectWrap = styled.div`
  width: 400px;
  @media (max-width: 768px) {
    width: 100%;
  }
`;
