import L from 'leaflet';
import WifiIcon from '../../assets/wifi.svg';
import WifiIconShadow from '../../assets/wifi_marker_shadow.svg';
import LocalLibraryIcon from '../../assets/book-reader.svg';
import BusIcon from '../../assets/bus.svg';
import BabyIcon from '../../assets/baby.svg';
import HotelIcon from '../../assets/hotel.svg';
import ParkingIcon from '../../assets/parking.svg';
import SchoolIcon from '../../assets/school.svg';
import MarkerIcon from '../../assets/map-marker-alt.svg';
import TramIcon from '../../assets/tram.svg';

import LocalLibraryShadow from '../../assets/book-reader_shadow.svg';
import BusShadow from '../../assets/bus_shadow.svg';
import BabyShadow from '../../assets/baby_shadow.svg';
import SchoolShadow from '../../assets/school_shadow.svg';
import MarkerShadow from '../../assets/map-marker_shadow.svg';
import TramShadow from '../../assets/tram_shadow.svg';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconUrl: MarkerIcon,
  shadowUrl: MarkerShadow,
  iconAnchor: [15, 30],
  iconSize: [30, 30],
  shadowSize: [33, 33],
});

export const wifiIcon = new L.Icon({
  iconUrl: WifiIcon,
  shadowUrl: WifiIconShadow,
  iconAnchor: [15, 30],
  iconSize: [30, 30],
  shadowSize: [33, 33],
});

export const libraryIcon = new L.Icon({
  iconUrl: LocalLibraryIcon,
  shadowUrl: LocalLibraryShadow,
  iconAnchor: [15, 30],
  iconSize: [30, 30],
  shadowSize: [33, 33],
});

export const busIcon = new L.Icon({
  iconUrl: BusIcon,
  shadowUrl: BusShadow,
  iconAnchor: [0, 0],
  iconSize: [15, 15],
  shadowSize: [17, 17],
});

export const tramIcon = new L.Icon({
  iconUrl: TramIcon,
  shadowUrl: TramShadow,
  iconAnchor: [0, 0],
  iconSize: [15, 15],
  shadowSize: [17, 17],
});

export const babyIcon = new L.Icon({
  iconUrl: BabyIcon,
  shadowUrl: BabyShadow,
  iconAnchor: [0, 0],
  iconSize: [30, 30],
  shadowSize: [33, 33],
});

export const hotelIcon = new L.Icon({
  iconUrl: HotelIcon,
  shadowUrl: WifiIconShadow,
  iconAnchor: [0, 0],
  iconSize: [15, 15],
  shadowSize: [33, 33],
});

export const parkingIcon = new L.Icon({
  iconUrl: ParkingIcon,
  shadowUrl: WifiIconShadow,
  iconAnchor: [0, 0],
  iconSize: [15, 15],
  shadowSize: [33, 33],
});

export const schoolIcon = new L.Icon({
  iconUrl: SchoolIcon,
  shadowUrl: SchoolShadow,
  iconAnchor: [0, 0],
  iconSize: [30, 30],
  shadowSize: [33, 33],
});
