export const options = [
  { value: 'wifipoints', label: 'WiFi access points' },
  { value: 'culturalpark', label: 'Cultural Park' },
  { value: 'libraries', label: 'Libraries' },
  { value: 'busstops', label: 'Bus stops' },
  { value: 'tramstops', label: 'Tram stops' },
  { value: 'parking', label: 'Parking areas' },
  {
    value: 'parentandkid',
    label: 'Parents and children clubs(up to 3 years old)',
  },
  { value: 'youthculturalcenter', label: 'Youth Culture Centers' },
  { value: 'musicschools', label: 'Music schools' },
  { value: 'primaryschools', label: 'Primary schools' },
  { value: 'highschools', label: 'High schools' },
  { value: 'vocationalschools', label: 'Vocational schools' },
  { value: 'colleges', label: 'Colleges' },
  { value: 'schoolcomplex', label: 'School complex' },
  { value: 'pnurseries', label: 'Public nurseries' },
  { value: 'npnurseries', label: 'Private nurseries' },
  { value: 'kindergartens', label: 'Kindergarten' },
  { value: 'dormitories', label: 'Dormitories' },
];

export const leisureOptions = [
  { value: 'wifipoints', label: 'WiFi access points' },
  { value: 'culturalpark', label: 'Cultural Park' },
  { value: 'libraries', label: 'Libraries' },
];

export const travelOptions = [
  { value: 'busstops', label: 'Bus stops' },
  { value: 'tramstops', label: 'Tram stops' },
  { value: 'parking', label: 'Parking areas' },
];

export const schoolOptions = [
  {
    value: 'parentandkid',
    label: 'Parents and children clubs(up to 3 years old)',
  },
  { value: 'youthculturalcenter', label: 'Youth Culture Centers' },
  { value: 'musicschools', label: 'Music schools' },
  { value: 'primaryschools', label: 'Primary schools' },
  { value: 'highschools', label: 'High schools' },
  { value: 'vocationalschools', label: 'Vocational schools' },
  { value: 'colleges', label: 'Colleges' },
  { value: 'schoolcomplex', label: 'School complex' },
  { value: 'pnurseries', label: 'Public nurseries' },
  { value: 'npnurseries', label: 'Private nurseries' },
  { value: 'kindergartens', label: 'Kindergarten' },
  { value: 'dormitories', label: 'Dormitories' },
];

export const groupedOptions = [
  { label: 'Leisure', options: leisureOptions },
  { label: 'Travel', options: travelOptions },
  { label: 'Education', options: schoolOptions },
];
