import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`

    *, *:before, *:after {
        -moz-box-sizing: border-box; 
        -webkit-box-sizing: border-box; 
        box-sizing: border-box;
        font-family: 'Open Sans', sans-serif;
        color: ${({ theme }) => theme.text};
    }

    body, html {
        margin: 0;
        padding: 0;
        overflow-x:hidden;
        background-color: #343434; 
    }
    
    html{
        font-size: 16px;
    }

    ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    	.leaflet-div-icon {
  background: transparent !important;
  border: none !important;
}

// custom scrollbar 

  &::-webkit-scrollbar {
    width: 10px;
    height: 10px;
  }

  &::-webkit-scrollbar-track {
    background: none;
  }

  &::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.secondary};
    border-radius: 10px;
  }

  &::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }) => theme.secondaryDarker};
  }
`;

export default GlobalStyle;
