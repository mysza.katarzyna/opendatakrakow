const theme = {
  primary: '#84DCCF',
  secondary: '#DBB3B1',
  secondaryDarker: '#C3807D',
  links: '#84DCCF',
  bg: '#343434',
  bgLight: '#525252',
  text: '#fff',
  button: '#56597A',
  inputBorder: '#2F2F2F',
  borderLight: '#626262',
  inputText: '#012145',
  error: '#c60000',
};

export default theme;
