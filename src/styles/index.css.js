import styled from 'styled-components';
import FrederickaTG from '../assets/fonts/FrederickatheGreat-Regular.ttf';
import OpenSans from '../assets/fonts/OpenSans-Regular.ttf';
import Comfortaa from '../assets/fonts/Comfortaa/Comfortaa-VariableFont_wght.ttf';
import Wave from '../assets/wave.svg';

export const Wrapper = styled.div`
  @font-face {
    font-family: 'Fredericka The Great';
    src: url(${FrederickaTG}) format('truetype');
  }

  @font-face {
    font-family: 'Open Sans';
    src: url(${OpenSans}) format('truetype');
  }

  @font-face {
    font-family: 'Comfortaa';
    src: url(${Comfortaa}) format('truetype');
  }

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: url(${Wave}) no-repeat;
  background-size: 100%;
  background-position: left bottom;
  padding: 20px 50px;
`;
